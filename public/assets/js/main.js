//FONCTION AUTOCOMPLETION

/*
autocompletion(
	true,  //true prends les premiers caractères.
	"ajax/moncompte_ajax.php",  //url du fichier php dont le code renvoie les data en json
	"POST",   //Type de tableau utilisé POST   GET
	"action=chargeCp",  // data envoyée à l'url
	false, //Désactive le debug  (alert)
	3, //nombre de caractères pour afficher la liste
	500,  //Délai d'affichage
	$("#code_postal"),  //elément html  contenant les données à afficher
	$( "#id_code_postal" ), //elément html cachée contenant les données à récupérer
	null  //fonction à déclencher une fois le choix fait
);
*/

function autocompletion(filtre,url,type,data,debug,minLength,delay,$label,$value,fonctionSelect){

	//Surcharge la méthode filter de autocomplete pour prendre les valeurs au début des caractères
	//si je ne le mets pas et si je saisis 504   il va trouver ce qui contient 504 :  0504 ou 00504 ou 5040
	if(filtre){
		$.ui.autocomplete.filter = function (array, term) {
		    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
		    return $.grep(array, function (value) {
		        return matcher.test(value.label || value.value || value);
		    });
		};
	}

	var dataJson=Array();
	$.ajax(
		{
			url:url,
			type:type,
			data:data,
			cache:false,
			success:function(data){
				if(debug) alert(data);
				dataJson=eval(data);
				$label.autocomplete({
			        minLength: minLength,
			        delay: delay,
			      	source: dataJson,
			      	select: function( event, ui ) {
				      	$label.val( ui.item.label );
				      	$value.val( ui.item.value );
				        //alert( ui.item.label + '-' + ui.item.valeur );	
				        if(fonctionSelect) fonctionSelect(); 
			        	return false;
			      	}					
				});
			},
			error:function(request){
				alert("Erreur Réseau:"+request.statusText);
				//alert("Chargement en cours");
			}
		}
	);

}
